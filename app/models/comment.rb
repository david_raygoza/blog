class Comment < ActiveRecord::Base
  belongs_to :post
  validates_presence_of(:post_id, :message => "Necesita estar asociado a un post")
  validates_presence_of(:body, :message => "Necesita ingresar un comentario")
end
