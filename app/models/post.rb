class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates_presence_of(:title, :message => "Introduzca el titulo")
    validates_presence_of(:body, :message => "Introduzca el post")
end
